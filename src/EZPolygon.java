import java.awt.*;
public class EZPolygon{
	private Polygon polly;
	
	public EZPolygon(Coordinate[] pts, int num){
		int[] xs = new int[num];
		int[] ys = new int[num];
		
		for(int i=0; i<num; i++){
			xs[i] = pts[i].getX();
			ys[i] = pts[i].getY();
		}		
		
		polly = new Polygon(xs,ys,num);		
	}
	public void fillPolygon(Graphics2D g, Color c){
		g.setColor(c);
		g.fillPolygon(polly);
	}
}
