/*
 * File: FractalFrame.java
 * Purpose:  To display your FractalPanel and save your fractal
 * Author: Mr. Reed
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

@SuppressWarnings("serial")
public class FractalFrame extends JFrame implements ActionListener{
	JLabel numLabel;
	JTextField numField;
	JButton draw;
	FractalPanel panel;
	JPanel buttonPanel;
	JScrollPane sp;
		
	/************************************************
	 * Constructor
	*************************************************/
	public FractalFrame(){
		super("Fractal Frame!");
		Container c = this.getContentPane();
		makeMenu();
		makeButtonPanel();
		c.add(buttonPanel, BorderLayout.NORTH);
		panel = new FractalPanel();
		sp = new JScrollPane(panel);
		sp.setPreferredSize(new Dimension(100,100));
		c.add(sp);
	}
	
	/*Preconditions: NONE
	 *Postconditions: the buttons and fields are created and added
	 *  	to buttonPanel
	 */
	private void makeButtonPanel(){
		buttonPanel = new JPanel();
		numLabel = new JLabel("Levels");
		numField = new JTextField(5);
		draw = new JButton("Draw");
		draw.addActionListener(this);
		
		buttonPanel.add(numLabel);
		buttonPanel.add(numField);
		buttonPanel.add(draw);
	}
	
	private void makeMenu()
   {
	 JMenuBar bar= new JMenuBar();
	 setJMenuBar(bar);

	 //****File Menu****
	 JMenu fileMenu=new JMenu("File");
	 fileMenu.setMnemonic('F');
	 bar.add(fileMenu);
	 

	 //Menu item for saving as jpg image format
	 JMenuItem save = new JMenuItem("Save File");
	 save.addActionListener(
	   new ActionListener(){
		 public void actionPerformed(ActionEvent e)
		 {
		   //filter to only show jpg files
		   MyFileFilter filter = new MyFileFilter("jpg");
		   File savedFile;
		   JFileChooser fileChooser = new JFileChooser("\\");
		   fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		   fileChooser.setFileFilter(filter);

		   int result = fileChooser.showSaveDialog(getParent());

		   if(result != JFileChooser.CANCEL_OPTION)
		   {
			 savedFile=fileChooser.getSelectedFile();
			 if(fileChooser.accept(savedFile))
			 { //if the user has chosen or entered a jpg file, save
			   panel.saveFile(savedFile);
			 }
			 else//prompt for correct file type and do not save
			   JOptionPane.showMessageDialog(null, "Must save to .jpg file");
		   }
		   else//cancel button was pressed
		   {
			   savedFile=null;
		   }
		 }
	   }
	 );//end addActionListener
	 fileMenu.add(save);
	 JMenuItem exit=new JMenuItem("Exit");
	 exit.addActionListener(
	   new ActionListener(){
		 public void actionPerformed(ActionEvent e)
		 {  //exit program
			 System.exit(0);
		 }
	   }
	 );//end addActionListener
	 fileMenu.add(exit);
   }
	
	
	/**************************************************
	 Handles the button event
	 **************************************************/
	public void actionPerformed(ActionEvent e){
		int x=0;
		try{//get integer from the number field
			x = Integer.parseInt(numField.getText());
		}
		catch(Exception ex){//bad number
			JOptionPane.showMessageDialog(null, "Enter an Integer!");
		}
		panel.setInput(x); //set the desired number of fractal levels
		panel.beginFract(); //draw the fractal
		panel.repaint();  // make system redraw the frame
	}

/*****************************************************
 * MAIN FUNCTION
 ******************************************************/	
	@SuppressWarnings("deprecation")
	public static void main(String[] args)
	{
		FractalFrame win = new FractalFrame();
		win.setSize(500,500);
		
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.show();
	}

}
