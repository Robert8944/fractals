// File:  FractalPanel.java
//Author: Mr. Reed and YOU :)
// Purpose:  a JPanel containing a BufferedImage object and a recursive
//           drawing function for making fun fractals


import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
//import com.sun.image.codec.jpeg.*;
import java.io.*;

@SuppressWarnings("serial")
public class FractalPanel extends JPanel{

	BufferedImage img;  // the image	
	int input, x=1;  // how many levels for the fractal
	Graphics2D g;
	Coordinate[] square1 = new Coordinate[4]; 
	Coordinate[] square2 = new Coordinate[4];
	Coordinate[] triangle1 = new Coordinate[3]; 
	Coordinate[] triangle2 = new Coordinate[3]; 
	

	//Constructor
	//Preconditions: NONE
	//Postconditions:  The panel is created 640x480 with a blank image	
	public FractalPanel(){
		super();
		this.setSize(new Dimension(1000,1000));
		this.setPreferredSize(new Dimension(this.getWidth(), this.getHeight()));
		input = 0; // no fractal yet
		img = new BufferedImage(this.getWidth(), this.getHeight(),  BufferedImage.TYPE_INT_RGB);
		//Make the image blank and white
		setBackground(Color.WHITE);
		Graphics2D ig = img.createGraphics();
		ig = img.createGraphics();
		ig.setBackground(Color.WHITE);
		ig.clearRect(0,0,img.getWidth(),img.getHeight());	
	}
	//Preconditions: None
	//Postconditions: The fractal is drawn to the bufferedImage and on the screen
	public void beginFract(){
		//the image is cleared (set to blank & white)
		g = img.createGraphics();
		g.setBackground(Color.WHITE);
		g.clearRect(0,0,img.getWidth(),img.getHeight());

/********************************************************
 * Place the FIRST call to recursive drawing function here!!
/********************************************************/
	
		g.setColor(Color.BLUE);
		////square
		Coordinate bottomleft = new Coordinate (450,600);
		Coordinate bottomright = new Coordinate (600,600);
		Coordinate topleft = new Coordinate (bottomright);
		Coordinate topright = new Coordinate (bottomleft);
		square1[0] = topleft;
		square1[1] = topright;
		square1[2] = bottomright;
		square1[3] = bottomleft;
		topleft.rotate(Math.PI/2, bottomleft);
		topright.rotate(-Math.PI/2, bottomright);
		//topleft.drawLine(bottomleft, g);
		//topleft.drawLine(topright, g);
		//bottomright.drawLine(topright, g);
		//bottomright.drawLine(bottomleft, g);
		EZPolygon firstSquare = new EZPolygon(square1, 4);
		firstSquare.fillPolygon(g, getrColor());
		
		////equillateral triangle
		Coordinate bottomleftt = new Coordinate (topleft);
		Coordinate bottomrightt = new Coordinate (topright);
		Coordinate topt = new Coordinate(bottomrightt);
		triangle1[0] = bottomleftt;
		triangle1[1] = bottomrightt;
		triangle1[2] = topt;
		topt.scale(Math.sqrt(2)/2, bottomleftt);
		topt.rotate(Math.PI/4, bottomleftt);
		//topt.drawLine(bottomleftt, g);
		//bottomrightt.drawLine(topt, g);
		EZPolygon firstTriangle = new EZPolygon( triangle1, 3);
		firstTriangle.fillPolygon(g, getrColor());
		
		
		recursiveDraw(input, bottomleftt, bottomrightt, topt, 1);		
	}

/************************************************
 * Here is your recursive drawing function!!!!
 * Notice that it is empty :)
 * But I have thrown you a bone by showing you some of the parameters
 * that it will need.
 * You will, of course, need some more parameters depending on the type
 * of fractal that you make.
 ***************************************/	
	/*Preconditions:
	 * 	g is the graphics object for the bufferedImage to which the fractal will be drawn
	 *  c is the desired color for this level of the fractal
	 *  num is the remaining number of desired fractal levels
	 *Postconditions: Part of the current level is drawn, and recurvsive calls are executed
	 *      that will draw the next level of the fractal 
	 */
	//private void recursiveDraw(Graphics2D g, Color c, int lvl, int x, int y, int sz){
	private void recursiveDraw(int lvl, Coordinate blt, Coordinate trt, Coordinate tt, int m){
		//stopping state
		if (lvl<=0)
			return;
		lvl--;
		
		
		
		//the left side
	////square
		Coordinate bottomlt = new Coordinate (blt);
		Coordinate bottomrt = new Coordinate (tt);
		Coordinate toplt = new Coordinate (bottomrt);
		Coordinate toprt = new Coordinate (bottomlt);
		square2[0] = toplt;
		square2[1] = toprt;
		square2[2] = bottomrt;
		square2[3] = bottomlt;
		toplt.rotate((m*Math.PI)/2, bottomlt);
		toprt.rotate((m*-Math.PI)/2, bottomrt);
		//toplt.drawLine(bottomlt, g);
		//toplt.drawLine(toprt, g);
		//bottomrt.drawLine(toprt, g);
		//bottomrt.drawLine(bottomlt, g);
		EZPolygon secondSquare = new EZPolygon(square2, 4);
		secondSquare.fillPolygon(g, getrColor());
	
		////equillateral triangles
		Coordinate bottomltt = new Coordinate (toplt);
		Coordinate bottomrtt = new Coordinate (toprt);
		//bottomltt.drawLine(bottomrtt, g);
		Coordinate tpt = new Coordinate(bottomrtt);
		triangle2[0] = bottomltt;
		triangle2[1] = bottomrtt;
		triangle2[2] = tpt;
		tpt.scale(Math.sqrt(2)/2,bottomltt);
		tpt.rotate(((m*Math.PI)/4), bottomltt);
		//tpt.drawLine(bottomltt, g);
		//bottomrtt.drawLine(tpt, g);
		EZPolygon secondTriangle = new EZPolygon( triangle2, 3);
		secondTriangle.fillPolygon(g, getrColor());
		
		
		recursiveDraw(lvl , bottomltt, bottomrtt, tpt, m);	
		//the right side
	////square
		Coordinate leftsideofthebottom = new Coordinate (trt);
		Coordinate rightsideofthebottom = new Coordinate (tt);
		Coordinate leftsideofthetop = new Coordinate (rightsideofthebottom);
		Coordinate rightsideofthetop = new Coordinate (leftsideofthebottom);
		square2[0] = leftsideofthetop;
		square2[1] = rightsideofthetop;
		square2[2] = rightsideofthebottom;
		square2[3] = leftsideofthebottom;
		leftsideofthetop.rotate((m*-Math.PI)/2, leftsideofthebottom);
		rightsideofthetop.rotate((m*Math.PI)/2, rightsideofthebottom);
		//leftsideofthetop.drawLine(leftsideofthebottom, g);
		//leftsideofthetop.drawLine(rightsideofthetop, g);
		//rightsideofthebottom.drawLine(rightsideofthetop, g);
		//rightsideofthebottom.drawLine(leftsideofthebottom, g);
		EZPolygon secondSquarepart2 = new EZPolygon(square2, 4);
		secondSquarepart2.fillPolygon(g, getrColor());
		
		////equillateral triangles
		Coordinate lefttrianglebottom = new Coordinate (leftsideofthetop);
		Coordinate righttrianglebottom = new Coordinate (rightsideofthetop);
		//lefttrianglebottom.drawLine(righttrianglebottom, g);
		Coordinate topofthetriangle = new Coordinate(righttrianglebottom);
		triangle2[0] = lefttrianglebottom;
		triangle2[1] = righttrianglebottom;
		triangle2[2] = topofthetriangle;
		topofthetriangle.scale(Math.sqrt(2)/2, lefttrianglebottom);
		topofthetriangle.rotate((m*-Math.PI)/4, lefttrianglebottom);
		//topofthetriangle.drawLine(lefttrianglebottom, g);
		//righttrianglebottom.drawLine(topofthetriangle, g);
		EZPolygon secondTrianglepart2 = new EZPolygon( triangle2, 3);
		secondTrianglepart2.fillPolygon(g, getrColor());
		
		recursiveDraw(lvl , lefttrianglebottom, righttrianglebottom, topofthetriangle, -m);	
	}
	public Color getrColor(){
		/*other two possible ways to do this function:
		//random COLOR genorator :)
		int red = (int)(Math.random()*256);
		int green = (int)(Math.random()*256);
		int blue = (int)(Math.random()*256);
		Color randy = new Color(red, green, blue);
		return randy;
		
		int x = (int)(Math.random()*3);
		*/
		x++;
		if (x>4)
			x=1;
		if (x==1)
			return Color.BLUE;
		else if(x==2)
			return Color.RED;
		else if(x==3)
			return Color.YELLOW;
		else
			return Color.GREEN;
		
	}
	
	
	
	
	//Mutator
	//Preconditions: NONE
	//Postconditions:  n is set to the desired number
	public void setInput(int tmpn){input = tmpn;}
	
	
	//Preconditions: none
	//Postconditions:  the fractal and panel are repainted
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D ig = (Graphics2D)g;
		ig.drawImage(img,0,0,this);
	}
	

	
	/*
	Preconditions: f is the file to be saved to
	Postconditions: The alteredImage is written to the specified file
	This file is called by the frame that contains the ImageObject
	*/
	public void saveFile(File f){/*

	  try{
		//initialize file stream to write to file
		FileOutputStream out = new FileOutputStream(f);
		//initialize encoder to transform image ot jpg fomat
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(img);
		param.setQuality(1.0f, false);
		encoder.setJPEGEncodeParam(param);
		encoder.encode(img);
	  }
	  catch(IOException e)
	  {
		JOptionPane.showMessageDialog(null, e.getMessage());
	  }
*/
	}
	
}
