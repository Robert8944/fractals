/*FILE:  MyFileFilter
  PURPOSE:  This is a simple FileFilter class used to quickly and simply
    create FileFilters for JFileChooser.
*/

import java.io.File;
import javax.swing.filechooser.*;
import java.util.Hashtable;
import java.util.Enumeration;

public class MyFileFilter extends FileFilter {

  @SuppressWarnings("rawtypes")
private Hashtable filters = null;

  //****CONSTRUCTOR****
  /*
  Precondition: extension is a valid file extension such as "jpg" or "gif".
    The period should not be included ("jpg" vs ".jpg")
  Postcondition: This specified extension is placed into a hash table which
    stores all of the specified extension names.
  */
  @SuppressWarnings({ "rawtypes", "unchecked" })
public MyFileFilter(String extension)
  {
        if(filters==null)
          filters=new Hashtable(5);
        //insert into hash table
        filters.put(extension, this);
   }
  /*
  Precondition: extensions is an array of valid file extension such as "jpg" or
    "gif".  The period should not be included ("jpg" vs ".jpg")
  Postcondition: This specified extensions are placed into a hash table which
    stores all of the specified extension names.
  */
   @SuppressWarnings({ "unchecked", "rawtypes" })
public MyFileFilter(String[] extensions)
   {
      if(filters==null)
        filters=new Hashtable(5);
      //insert each element of the array into hash table
      for(int i=0; i<extensions.length;i++)
        filters.put(extensions[i], this);
   }

  /*
  Precondition: file is a valid file name
  Postcondition: The function returns the file's extension.
  */
  public String getExtension(File file)
  {
    if(file != null)
    {
      //get name of file
	    String name = file.getName();
      //find position of period
	    int i = name.lastIndexOf('.');
	    if(i>0 && i<name.length()-1)
        return name.substring(i+1);

        //return substring representing extension
    }
    return null;
  }
  /*
  Precondition: none
  Postcondition: A string listing all of the extension names
    is returned
  */
  @SuppressWarnings("rawtypes")
public String getDescription()
  {
    String descs=new String();
    //create a list of all extensions
    Enumeration list=filters.keys();
    //create comma separated list of extensions
    descs=descs+(String)list.nextElement();
    while (list.hasMoreElements())
      descs=descs+", " + (String)list.nextElement();
    return descs;

  }
  /*
  Precondition: f is a valid file
  Postcondition: If f's extension is one of the extensions that this
    filter accepts, the function returns true.  Otherwise, false is returned.
  */
  public boolean accept(File f)
  {
      if(f != null)
      {
        //accept all directories
	      if(f.isDirectory())
        {
    		  return true;
        }
        //find file extension
	      String ext = getExtension(f);
        //if extension is present in hash table, return true
	      if(ext != null && filters.get(getExtension(f)) != null)
        {
    	  	return true;
	      }
      }
      return false;
  }
}
