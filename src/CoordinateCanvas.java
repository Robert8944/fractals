import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class CoordinateCanvas extends JPanel{
	public CoordinateCanvas(){
		super();	
	}		
	public void paint(Graphics g){
	// PUT YOUR CODE IN HERE :)
		
		////equillateral triangles
		Coordinate aaron = new Coordinate (250, 10);
		Coordinate coree = new Coordinate (250,250);
		aaron.drawLine(coree, g, Color.BLUE);
		Coordinate coree2 = new Coordinate(coree);
		coree2.rotate(-Math.PI/3, aaron);
		coree2.drawLine(aaron, g, Color.BLUE);
		coree.drawLine(coree2, g, Color.BLUE);
		
		////square
		Coordinate jesus = new Coordinate (10,10);
		Coordinate virginmary = new Coordinate (10,20);
		Coordinate saintmichaelthearchangel = new Coordinate (20,20);
		Coordinate popejohnpaulthesecond = new Coordinate (20,10);
		virginmary.rotate(Math.PI/3,jesus);
		saintmichaelthearchangel.rotate(Math.PI/3,jesus);
		popejohnpaulthesecond.rotate(Math.PI/3,jesus);
		jesus.drawLine(virginmary, g);
		jesus.drawLine(popejohnpaulthesecond, g);
		saintmichaelthearchangel.drawLine(popejohnpaulthesecond, g);
		saintmichaelthearchangel.drawLine(virginmary, g);
		
		////pentagon
		Coordinate bob = new Coordinate(500,500);
		Coordinate beb = new Coordinate(475,525);
		Coordinate bib = new Coordinate(525,525);
		Coordinate bab = new Coordinate(488,550);
		Coordinate bub = new Coordinate(512,550);
		bob.drawLine(beb, g);
		bob.drawLine(bib, g);
		beb.drawLine(bab, g);
		bib.drawLine(bub, g);
		bub.drawLine(bab, g);
		
		////bowtie
		Coordinate tiger = new Coordinate (250,500);
		Coordinate elephant = new Coordinate (250,525);
		Coordinate whale = new Coordinate (275,500);
		Coordinate seal = new Coordinate (275,525);
		tiger.drawLine(elephant, g);
		tiger.drawLine(seal, g);
		whale.drawLine(seal, g);
		whale.drawLine(elephant, g);
		
		////45-45-90
		Coordinate table = new Coordinate(700,500);
		Coordinate chair = new Coordinate(725,500);
		Coordinate seat = new Coordinate (700,475);
		chair.rotate(Math.PI/5, table);
		seat.rotate(Math.PI/5, table);
		table.drawLine(chair, g);
		table.drawLine(seat, g);
		chair.drawLine(seat, g);
		
		////star
		Coordinate bobo = new Coordinate(600,500);
		Coordinate bebo = new Coordinate(575,525);
		Coordinate bebe = new Coordinate(625,525);
		Coordinate bobe = new Coordinate(588,550);
		Coordinate buba = new Coordinate(612,550);
		bobo.drawLine(bobe, g);
		bobo.drawLine(buba, g);
		bebo.drawLine(bebe, g);
		bobe.drawLine(bebe, g);
		buba.drawLine(bebo, g);
		
		////30-60-90
		Coordinate fractal = new Coordinate(500,400);
		Coordinate mathmaticalshape = new Coordinate(500,800);
		Coordinate point = new Coordinate(800,400);
		mathmaticalshape.rotate(Math.PI/5, fractal);
		point.rotate(Math.PI/5, fractal);
		fractal.drawLine(point, g);
		fractal.drawLine(mathmaticalshape, g);
		point.drawLine(mathmaticalshape, g);
		
		////four equilateral triangles whose bases form a square
		Coordinate x = new Coordinate (50,50);
		Coordinate y = new Coordinate (50,100);
		Coordinate z = new Coordinate (100,100);
		Coordinate p = new Coordinate (100,50);
		Coordinate a = new Coordinate (0,50);
		Coordinate b = new Coordinate (0,100);
		Coordinate c = new Coordinate (100,150);
		Coordinate d = new Coordinate (150,50);
		a.rotate(-Math.PI/2,x);
		a.rotate(-Math.PI/5.5, x);
		b.rotate(-Math.PI/5.5, y);
		c.rotate(-Math.PI/5.5, z);
		d.rotate(-Math.PI/5.5, p);
		x.drawLine(y, g);
		x.drawLine(p, g);
		z.drawLine(p, g);
		z.drawLine(y, g);
		a.drawLine(x, g);
		a.drawLine(p, g);
		b.drawLine(x, g);
		b.drawLine(y, g);
		c.drawLine(y, g);
		c.drawLine(z, g);
		d.drawLine(z, g);
		d.drawLine(p, g);
		
		
		
		
		

		
	}
	
	public static void main(String[] args){
		JFrame app = new JFrame("Coordinate Canvas");
		app.setBackground(Color.WHITE);
		app.getContentPane().add(new CoordinateCanvas());
		app.setSize(500,500);
		app.setVisible(true);
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	}
}
